FROM python:3.13-slim

ENV LIBRARY_DIR=/music

# install dependencies
RUN apt-get update
RUN apt-get install -y ffmpeg atomicparsley

# install app
COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

# prepare audio folder
COPY ./app /app
RUN mkdir temp_audio
RUN chmod 777 temp_audio
RUN mkdir -p /.cache
RUN chmod 777 /.cache

# Remove dependencies
RUN apt-get clean autoclean
RUN apt-get autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/

VOLUME ["/music"]

WORKDIR /app

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
