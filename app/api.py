"""Module providing API to downloading Audio stream from Youtube like services"""

import logging
from tokenize import String

import yt_dlp
from fastapi import APIRouter, BackgroundTasks
from requests import post
from yt_dlp.utils import YoutubeDLError
from download_logger import DownloadLogger
from models import AudioDownloadRequest

router = APIRouter()
logger = logging.getLogger("uvicorn.error")

library_dir = ""
home_assistant_notification = ""
home_assistant_address = ""
home_assistant_token = ""
home_assistant_mobile_device = ""

success_message = "Finished downloading {} - {}."
failure_message = "Failed to download {} - {}."


def progress_hook(d):
    """Function to handle hook to log progress."""

    if d["status"] == "finished":
        file_name = d["filename"]
        logger.info("Done downloading: %s.", file_name)


@router.post("/api", status_code=200)
async def download_video(
    request: AudioDownloadRequest, background_tasks: BackgroundTasks
):
    """Function to define download API endpoint"""

    logger.info(
        "Submitting task to download %s - %s", request.artist, request.title
    )

    background_tasks.add_task(download_audio, request)

    return {"message": "Download task submitted."}


def download_audio(request: AudioDownloadRequest):
    """Function to download the audio stream from a public video."""

    global home_assistant_notification
    global library_dir

    logger.info("Downloading audio from video url: %s", request.url)
    logger.info("Using Music Library directory: %s", library_dir)

    file_name = f"{library_dir}/{request.artist}/{request.album}/{request.artist} - {request.title}.%(ext)s"

    ydl_opts = {
        "compat_opts": {"embed-thumbnail-atomicparsley"},
        "extract_flat": "discard_in_playlist",
        "format": "m4a/bestaudio[audio_channels=2]/best",
        "logger": DownloadLogger(),
        "progress_hooks": [progress_hook],
        "quiet": True,
        "fragment_retries": 10,
        "ignoreerrors": "only_download",
        "outtmpl": file_name,
        "writethumbnail": True,
        "postprocessors": [
            {"key": "FFmpegMetadata"},
            {
                "key": "FFmpegExtractAudio",
                "nopostoverwrites": False,
                "preferredcodec": "best",
                "preferredquality": "5",
            },
            {"already_have_thumbnail": False, "key": "EmbedThumbnail"},
            {
                "key": "FFmpegConcat",
                "only_multi_video": True,
                "when": "playlist",
            },
        ],
    }

    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(request.url, download=False)
        logger.info(
            "Release date of the track: %s", info_dict.get("upload_date", "N/A")
        )

        upload_date = info_dict.get("upload_date")
        formatted_upload_date = (
            f"{upload_date[:4]}-{upload_date[4:6]}-{upload_date[6:]}"
            if upload_date
            else ""
        )

    try:
        ydl_opts["postprocessor_args"] = {
            "ffmpeg": [
                "-metadata",
                f"album={request.album}",
                "-metadata",
                f"artist={request.artist}",
                "-metadata",
                f"title={request.title}",
                "-metadata",
                f"date={formatted_upload_date}",
            ]
        }

        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            ydl.download([request.url])

    except YoutubeDLError:
        if home_assistant_notification == "yes":
            send_notification(request, failure_message)

        return

    if home_assistant_notification == "yes":
        send_notification(request, success_message)


def send_notification(request: AudioDownloadRequest, message: String):
    """Function to send Home-Assistant notification when download is completed."""

    url = f"{home_assistant_address}/api/services/notify/{home_assistant_mobile_device}"

    headers = {
        "Authorization": f"Bearer {home_assistant_token}",
        "content-type": "application/json",
    }

    body = {
        "title": "Audio Downloader API",
        "message": message.format(request.artist, request.title),
    }

    post(url, headers=headers, json=body, timeout=5)

    logger.info("Notification for %s - %s sent.", request.artist, request.title)
