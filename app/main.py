""" Main module to start the application"""

import os
import api
from fastapi import FastAPI

app = FastAPI()

app.include_router(api.router)

api.library_dir = os.getenv("LIBRARY_DIR", '.')
api.home_assistant_notification = os.getenv("HOME_ASSISTANT_NOTIFICATION", '.')
api.home_assistant_address = os.getenv("HOME_ASSISTANT_ADDRESS", '.')
api.home_assistant_mobile_device = os.getenv("HOME_ASSISTANT_MOBILE_DEVICE", '.')
api.home_assistant_token = os.getenv("HOME_ASSISTANT_TOKEN", '.')

if __name__ == "__main__":
    import uvicorn

    uvicorn.run("main:app", host="0.0.0.0", reload=True)
